/*
    task
    1. Напишите функцию подготовки строки, которая заполняет шаблон данными из указанного объекта
    2. Пришлите код целиком, чтобы можно его было проверить
    3. Придерживайтесь code style текущего задания
    4. По необходимости - можете дописать код, методы
    5. Разместите код в гите и пришлите ссылку
*/

/**
 * Класс для работы с API
 *
 * @author		User Name 
 * @version		v.1.0 (dd/mm/yyyy)
 */
class Api
{
  constructor() 
  {

  }


  get_api_path(object: Record<string, any>, template: string) {
    let result = template;

    for (const key in object) {
      if (template.includes(`%${key}%`)) {
        result = result.replace(new RegExp(`%${key}%`, 'gi'), this.escape_spaces(object[key]));
      }
    }

    return result;
  }

  escape_spaces(value: string) {
    return String(value).replace(/\s/gi, "%20");
  }
}
 
 
let user =
{
  id		: 20,
  name	: 'John Dow',
  role	: 'QA',
  salary	: 100
};

let api_path_templates =
[
  "/api/items/%id%/%name%",
  "/api/items/%id%/%role%",
  "/api/items/%id%/%salary%"
];

let api = new Api();

let api_paths = api_path_templates.map((api_path_template) =>
{
  return api.get_api_path(user, api_path_template);
});

// Ожидаемый результат
let expected_result = ["/api/items/20/John%20Dow","/api/items/20/QA","/api/items/20/100"];

console.log({
  valid: JSON.stringify(api_paths) === JSON.stringify(expected_result),
  result: JSON.stringify(api_paths)
});

 
 